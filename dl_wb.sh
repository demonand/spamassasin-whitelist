#!/bin/bash

cd /usr/local/spamassassin-wb

if [ -f local ]; then
source local
fi

git clone https://gitlab.com/demonand/spamassasin-whitelist.git
#sed -i $REG spamassasin-whitelist/white.cf
cp spamassasin-whitelist/*.cf /etc/mail/spamassassin/
cp spamassasin-whitelist/dl_wb.sh .
chmod 777 dl_wb.sh
rm -rf spamassasin-whitelist


/usr/bin/systemctl reload amavisd.service
